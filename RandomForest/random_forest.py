"""
Random Forest Lab

Lee Woodside
MATH 403
09.28.2021
"""
import graphviz
import os
import time
import math
import random
from statistics import mode
import numpy as np
from uuid import uuid4
from sklearn.ensemble import RandomForestClassifier

# Problem 1
class Question:
    """Questions to use in construction and display of Decision Trees.
    Attributes:
        column (int): which column of the data this question asks
        value (int/float): value the question asks about
        features (str): name of the feature asked about
    Methods:
        match: returns boolean of if a given sample answered T/F"""
    
    def __init__(self, column, value, feature_names):
        self.column = column
        self.value = value
        self.features = feature_names[self.column]
    
    def match(self,sample):
        """Returns T/F depending on how the sample answers the question
        Parameters:
            sample ((n,), ndarray): New sample to classify
        Returns:
            (bool): How the sample compares to the question"""
        return sample[self.column] >= self.value
        
    def __repr__(self):
        return "Is %s >= %s?" % (self.features, str(self.value))
    
def partition(data,question):
    """Splits the data into left (true) and right (false)
    Parameters:
        data ((m,n), ndarray): data to partition
        question (Question): question to split on
    Returns:
        left ((j,n), ndarray): Portion of the data matching the question
        right ((m-j, n), ndarray): Portion of the data NOT matching the question
    """
    mask = np.array([question.match(i) for i in data])
    left = data[mask]
    right = data[~mask]

    if len(right) == 0:
        right = None
    if len(left) == 0:
        left = None

    return left, right

    
#Problem 2    
def gini(data):
    """Return the Gini impurity of given array of data.
    Parameters:
        data (ndarray): data to examine
    Returns:
        (float): Gini impurity of the data"""
    #check if only one row in data
    if data is None or len(data) == 1 or len(data) == 0:
        return 0.0

    labels, counts = np.unique(data[:, -1], return_counts=True)
    n = len(data)
    return 1 - np.sum([(i / n)**2 for i in counts])

def info_gain(left,right,G):
    """Return the info gain of a partition of data.
    Parameters:
        left (ndarray): left split of data
        right (ndarray): right split of data
        G (float): Gini impurity of unsplit data
    Returns:
        (float): info gain of the data"""
    if left is None:
        D_l = 0 
    else:
        D_l = len(left)
    if right is None:
        D_r = 0 
    else:
        D_r = len(right)
    D = D_l + D_r
    return G - (D_l/D * gini(left) + D_r/D * gini(right))
    
# Problem 3, Problem 7
def find_best_split(data, feature_names, min_samples_leaf=5, random_subset=False):
    """Find the optimal split
    Parameters:
        data (ndarray): Data in question
        feature_names (list of strings): Labels for each column of data
        min_samples_leaf (int): minimum number of samples per leaf
        random_subset (bool): for Problem 7
    Returns:
        (float): Best info gain
        (Question): Best question"""
    feature_names = list(feature_names[:-1])
    n = math.floor(np.sqrt(len(feature_names) - 1))
    G = gini(data)
    max_Q = Question(0, data[0,0], feature_names)
    max_info = -1
    if random_subset:
        feature_names = random.sample(feature_names, n)
    for i in range(len(feature_names)):
        for j in range(len(data)):
            Q = Question(i, data[j,i], feature_names)
            left, right = partition(data, Q)
            if left is None:
                D_l = 0 
            else:
                D_l = len(left)
            if right is None:
                D_r = 0 
            else:
                D_r = len(right)
            info = info_gain(left, right, G)
            if info > max_info and D_l >= min_samples_leaf and D_r >= min_samples_leaf:
                max_Q = Q
                max_info = info
    return max_info, max_Q



# Problem 4
class Leaf:
    """Tree leaf node
    Attribute:
        prediction (dict): Dictionary of labels at the leaf"""
    def __init__(self,data):
        classes, counts = np.unique(data[:,-1], return_counts=True)
        self.prediction = dict(zip(classes, counts))
    

class Decision_Node:
    """Tree node with a question
    Attributes:
        question (Question): Question associated with node
        left (Decision_Node or Leaf): child branch
        right (Decision_Node or Leaf): child branch"""
    def __init__(self, question, right_branch, left_branch):
        self.question = question
        self.right = right_branch
        self.left = left_branch

## Code to draw a tree
def draw_node(graph, my_tree):
    """Helper function for drawTree"""
    node_id = uuid4().hex
    #If it's a leaf, draw an oval and label with the prediction
    if isinstance(my_tree, Leaf):
        graph.node(node_id, shape="oval", label="%s" % my_tree.prediction)
        return node_id
    else: #If it's not a leaf, make a question box
        graph.node(node_id, shape="box", label="%s" % my_tree.question)
        left_id = draw_node(graph, my_tree.left)
        graph.edge(node_id, left_id, label="T")
        right_id = draw_node(graph, my_tree.right)    
        graph.edge(node_id, right_id, label="F")
        return node_id

def draw_tree(my_tree):
    """Draws a tree"""
    #Remove the files if they already exist
    for file in ['Digraph.gv','Digraph.gv.pdf']:
        if os.path.exists(file):
            os.remove(file)
    graph = graphviz.Digraph(comment="Decision Tree")
    draw_node(graph, my_tree)
    graph.render(view=True) #This saves Digraph.gv and Digraph.gv.pdf

# Prolem 5
def build_tree(data, feature_names, min_samples_leaf=5, max_depth=4, current_depth=0, random_subset=False):
    """Build a classification tree using the classes Decision_Node and Leaf
    Parameters:
        data (ndarray)
        feature_names(list or array)
        min_samples_leaf (int): minimum allowed number of samples per leaf
        max_depth (int): maximum allowed depth
        current_depth (int): depth counter
        random_subset (bool): whether or not to train on a random subset of features
    Returns:
        Decision_Node (or Leaf)"""
    depth = current_depth
    
    if data is None or current_depth == max_depth or len(data) < min_samples_leaf:
        return(Leaf(data))

    info, Q = find_best_split(data, feature_names, min_samples_leaf, random_subset)
    
    if info <= 0.0:
        return(Leaf(data))

    else:
        left, right = partition(data, question=Q)
        return Decision_Node(Q, 
                build_tree(right, feature_names, current_depth = depth+1), 
                build_tree(left, feature_names, current_depth = depth+1))

# Problem 6
def predict_tree(sample, my_tree):
    """Predict the label for a sample given a pre-made decision tree
    Parameters:
        sample (ndarray): a single sample
        my_tree (Decision_Node or Leaf): a decision tree
    Returns:
        Label to be assigned to new sample"""
     # Base case: if leaf we return prediction
    if isinstance(my_tree, Leaf):
        return my_tree.prediction
    # Recursive case checking left and right 
    if my_tree.question.match(sample):
        return predict_tree(sample, my_tree.left)
    return predict_tree(sample, my_tree.right)
    

    
def analyze_tree(dataset,my_tree):
    """Test how accurately a tree classifies a dataset
    Parameters:
        dataset (ndarray): Labeled data with the labels in the last column
        tree (Decision_Node or Leaf): a decision tree
    Returns:
        (float): Proportion of dataset classified correctly"""
    predictions = [1 if max(predict_tree(sample, my_tree), key=predict_tree(sample, my_tree).get) == sample[-1] else 0 for sample in dataset]
    return(np.mean(predictions))

# Problem 7
def predict_forest(sample, forest):
    """Predict the label for a new sample, given a random forest
    Parameters:
        sample (ndarray): a single sample
        forest (list): a list of decision trees
    Returns:
        Label to be assigned to new sample"""
    predictions = [max(predict_tree(sample, tree), key=predict_tree(sample, tree).get) for tree in forest]
    return mode(predictions)

def analyze_forest(dataset,forest):
    """Test how accurately a forest classifies a dataset
    Parameters:
        dataset (ndarray): Labeled data with the labels in the last column
        forest (list): list of decision trees
    Returns:
        (float): Proportion of dataset classified correctly"""
    correct = [1 if predict_forest(sample, forest) == sample[-1] else 0 for sample in dataset]
    return(np.mean(correct))

# Problem 8
def prob8():
    """Use the file parkinsons.csv to analyze a 5 tree forest.
    
    Create a forest with 5 trees and train on 100 random samples from the dataset.
    Use 30 random samples to test using analyze_forest() and SkLearn's 
    RandomForestClassifier.
    
    Create a 5 tree forest using 80% of the dataset and analzye using 
    RandomForestClassifier.
    
    Return three tuples, one for each test.
    
    Each tuple should include the accuracy and time to run: (accuracy, running time) 
    """
    # Load in the data
    park_data = np.loadtxt('parkinsons.csv', delimiter=',')[:,1:]
    idx = np.random.randint(len(park_data)-1, size=130)
    # Load in feature names
    features = np.loadtxt('parkinsons_features.csv', delimiter=',', dtype=str, comments=None)
    rand_samp = park_data[idx,:]
    train = rand_samp[:100]
    test = rand_samp[100:]
    my_forest = []

    start_train = time.time()
    for i in range(5):
        rand_samp = park_data[idx,:]
        
        my_tree = build_tree(train,features,min_samples_leaf=15,random_subset=True)
        my_forest.append(my_tree)

    #analyze My Forest 
    acc = analyze_forest(test, my_forest)
    end_train = time.time()
    time_train = end_train - start_train
    
    first = (acc, time_train)

    #Scikit Stuff for the 130
    forest1 = RandomForestClassifier(n_estimators=5, max_depth=4, min_samples_leaf=15)
    # Shuffle the data
    shuffled = np.random.permutation(park_data)
    # Fit the model to your data, passing the labels in as the second argument
    forest1.fit(train[:,:-1], train[:,-1])
    # Test the accuracy with the testing set
    scikit_start = time.time()
    scikit_acc = forest1.score(test[:,:-1], test[:,-1])
    scikit_end = time.time()
    second = (scikit_acc, scikit_end-scikit_start)

    #Scikit for entire data
    total_len = len(park_data)
    forest2 = RandomForestClassifier()
    # Shuffle the data
    shuffled = np.random.permutation(park_data)
    # Fit the model to your data, passing the labels in as the second argument
    forest2.fit(park_data[0:int(total_len*0.8), :-1], park_data[:int(total_len*0.8),-1])
    # Test the accuracy with the testing set
    start = time.time()
    scikit_acc_2 = forest2.score(park_data[int(total_len*0.8):,:-1], park_data[int(total_len*0.8):,-1])
    end = time.time()
    third = (scikit_acc_2, end - start)

    return(first, second, third)

    
# if __name__ == '__main__':
#     # data = np.array([[0,5,125,0], [1,0,100,0], [0,0,70,0], [1,3,120,0], [0,0,95,0]])
#     # question = Question(2, 85, 'Income')
#     # part = partition(data, question)
#     # print(part[0], part[1])

#     # Load in the data
#     animals = np.loadtxt('animals.csv', delimiter=',')
#     # Load in feature names
#     features = np.loadtxt('animal_features.csv', delimiter=',', dtype=str, comments=None)
#     # Load in sample names
#     names = np.loadtxt('animal_names.csv', delimiter=',', dtype=str)
#     # Test your functions
    
#     forest = []
#     train = animals[:80]
#     test = animals[80:]
#     for i in range(5):
#         np.random.shuffle(animals) 
#         my_tree = build_tree(animals, features, random_subset=True)
#         forest.append(my_tree)

#     print(prob8())
    # print(analyze_forest(train, forest))