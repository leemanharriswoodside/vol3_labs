import numpy as np
import pandas as pd
from sklearn.decomposition import NMF
from sklearn.metrics import mean_squared_error as MSE


class NMFRecommender:

    def __init__(self, random_state=15, tol=1e-3, maxiter=200, rank=3):
        """The parameter values for the algorithm"""
        self.random_state = random_state
        self.tol = tol
        self.maxiter = maxiter
        self.rank = rank
       
    def initialize_matrices(self, m, n):
        """Initialize the W and H matrices"""
        np.random.seed(self.random_state)

        W = np.random.uniform(0, 1, size=(m,self.rank))
        H = np.random.uniform(0, 1, size=(self.rank,n))

        return W, H

    def compute_loss(self, V, W, H):
        """Computes the loss of the algorithm according to the frobenius norm"""
        A = V - W @ H
        return np.sqrt(np.sum(np.diag(A.T @ A)))

    def update_matrices(self, V, W, H):
        """The multiplicative update step to update W and H"""
        H_new = H * ((W.T @ V)/(W.T @ W @ H))
        W_new = W * ((V @ H_new.T)/(W @ H_new @ H_new.T))

        return W_new, H_new

      
    def fit(self, V):
        """Fits W and H weight matrices according to the multiplicative update 
        algorithm. Return W and H"""
        m,n = V.shape
        W, H = self.initialize_matrices(m, n)
        loss = np.inf
        i=0

        while i < self.maxiter and loss > self.tol:
            W, H = self.update_matrices(V, W, H)
            loss = self.compute_loss(V, W, H)
            i+=1

        return W, H

    def reconstruct(self, W, H):
        """Reconstructs the V matrix for comparison against the original V 
        matrix"""

        return W @ H
        
def prob4():
    """Run NMF recommender on the grocery store example"""
    V = np.array([[0,1,0,1,2,2],
                  [2,3,1,1,2,2],
                  [1,1,1,0,1,1],
                  [0,2,3,4,1,1],
                  [0,0,0,0,1,0]])
    m,n = V.shape
    nmf = NMFRecommender(rank=2)
    W, H = nmf.fit(V)
    V = nmf.reconstruct(W, H)
    mask = H[1,:][H[1,:] > H[0,:]]
    return(W, H, len(mask))

def prob5():
    """Calculate the rank and run NMF
    """
    V = pd.read_csv('artist_user.csv')
    benchmark = np.linalg.norm(V) * .0001
    rank = 11

    rmse = np.inf

    while rmse > benchmark and rank < 100:
        rank += 1
        nmf = NMF(n_components=rank, init='random', random_state=0)
        W = nmf.fit_transform(V)
        H = nmf.components_
        new_V = W @ H

        rmse = np.sqrt(MSE(V, new_V))

    reconstructed_df = pd.DataFrame(data=new_V, index=V.index, columns=V.columns)

    return rank, reconstructed_df



def discover_weekly(re_X, userID):
    """
    Create the recommended weekly 30 list for a given user
    """
    X = pd.read_csv('artist_user.csv', index_col=0, header=0)
    artists = pd.read_csv('artists.csv', index_col=0, header=0)

    row = np.array(re_X.loc[userID])
    row_sort = np.argsort(row)[::-1]

    count = 0
    recommendations = []

    for i in range(len(row_sort)):
        artist_id = re_X.loc[userID].index[row_sort[i]]
        if X.loc[userID, str(artist_id)] == 0:
            recommendations.append(artists.loc[artist_id][0])
            count += 1
        if count == 30:
            break

    return recommendations
