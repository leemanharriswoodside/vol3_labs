import numpy as np
import pandas as pd
from scipy import stats
from sklearn.base import ClassifierMixin
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split



class NaiveBayesFilter(ClassifierMixin):
    '''
    A Naive Bayes Classifier that sorts messages in to spam or ham.
    '''

    def __init__(self):
        return 

    def fit(self, X, y):
        '''
        Create a table that will allow the filter to evaluate P(H), P(S)
        and P(w|C)

        Parameters:
            X (pd.Series): training data
            y (pd.Series): training labels
        '''
        # create dataframe 
        unique_words = pd.Series(np.concatenate([x.split() for x in X])).value_counts().index
        print(unique_words)
        
    def predict_proba(self, X):
        '''
        Find P(C=k|x) for each x in X and for each class k by computing
        P(C=k)P(x|C=k)

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,2): Probability each message is ham, spam
                0 column is ham
                1 column is spam
        '''

        raise NotImplementedError('Problem 2 incomplete')

    def predict(self, X):
        '''
        Use self.predict_proba to assign labels to X,
        the label will be a string that is either 'spam' or 'ham'

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,): label for each message
        '''
        
        raise NotImplementedError('Problem 3 incomplete')
        
    def predict_log_proba(self, X):
        '''
        Find ln(P(C=k|x)) for each x in X and for each class k

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,2): Probability each message is ham, spam
                0 column is ham
                1 column is spam
        '''

        raise NotImplementedError('Problem 4 incomplete')
        

    def predict_log(self, X):
        '''
        Use self.predict_log_proba to assign labels to X,
        the label will be a string that is either 'spam' or 'ham'

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,): label for each message
        '''
        
        raise NotImplementedError('Problem 4 incomplete')


class PoissonBayesFilter(ClassifierMixin):
    '''
    A Naive Bayes Classifier that sorts messages in to spam or ham.
    This classifier assumes that words are distributed like 
    Poisson random variables
    '''

    def __init__(self):
        return

    
    def fit(self, X, y):
        '''
        Uses bayesian inference to find the poisson rate for each word
        found in the training set. For this we will use the formulation
        of l = rt since we have variable message lengths.

        This method creates a tool that will allow the filter to 
        evaluate P(H), P(S), and P(w|C)


        Parameters:
            X (pd.Series): training data
            y (pd.Series): training labels
        
        Returns:
            self: this is an optional method to train
        '''

        raise NotImplementedError('Problem 6 incomplete')
    
    def predict_proba(self, X):
        '''
        Find P(C=k|x) for each x in X and for each class

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,2): Probability each message is ham or spam
                column 0 is ham, column 1 is spam 
        '''

        raise NotImplementedError('Problem 7 incomplete')

    def predict(self, X):
        '''
        Use self.predict_proba to assign labels to X

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,): label for each message
        '''
        
        raise NotImplementedError('Problem 7 incomplete')



def sklearn_method(X_train, y_train, X_test):
    '''
    Use sklearn's methods to transform X_train and X_test, create a
    naïve Bayes filter, and classify the provided test set.

    Parameters:
        X_train (pandas.Series): messages to train on
        y_train (pandas.Series): labels for X_train
        X_test  (pandas.Series): messages to classify

    Returns:
        (ndarray): classification of X_test
    '''

    raise NotImplementedError('Problem 8 incomplete')


if __name__ == '__main__':
    # load in the sms dataset
    df = pd.read_csv('sms_spam_collection.csv')
    # separate the data into the messages and labels
    X = df.Message
    y = df.Label
    # split the data into test and train sets
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.7)

    print(X[:300], y[:300])

    NBF = NaiveBayesFilter()
    NBF.fit(X[:300], y[:300])
    