"""Volume 3: Web Scraping.
<Lee Woodside>
<math405>
<1-17-2022>
"""

import requests
import re
from os.path import exists
from bs4 import BeautifulSoup
import codecs
import pandas as pd

# Problem 1
def prob1():
    """Use the requests library to get the HTML source for the website 
    http://www.example.com.
    Save the source as a file called example.html.
    If the file already exists, do not scrape the website or overwrite the file.
    """
    response = requests.get("http://www.example.com")
    if(exists('example.html') == False):
        with open('example.html', 'w') as f:
            f.write(response.text)


    
# Problem 2
def prob2(code):
    """Return a list of the names of the tags in the given HTML code.
    Parameters:
        code (str): A string of html code
    Returns:
        (list): Names of all tags in the given code"""
    tags = BeautifulSoup(code, 'html.parser').find_all(True)

    tags = [tags[i].name for i in range(len(tags))]

    return tags


# Problem 3
def prob3(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Return the
    text of the first <a> tag and whether or not it has an href
    attribute.
    Parameters:
        filename (str): Filename to open
    Returns:
        (str): text of first <a> tag
        (bool): whether or not the tag has an 'href' attribute
    """
    f = codecs.open(filename, 'r')
    soup = BeautifulSoup(f.read(), 'html.parser')
    a_tag = soup.a
    attrs = a_tag.attrs

    if 'href' in attrs:
        return a_tag.string, True
    else:
        return a_tag.string, False


# Problem 4
def prob4(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    f = codecs.open(filename, 'r')
    soup = BeautifulSoup(f.read(), 'html.parser')

    tag_1 = soup.find(string='Thursday, January 1, 2015').parent
    prev_day = soup.find(string='« Previous Day').parent
    next_day = soup.find(string='Next Day »').parent

    table_tag = soup.find(name="table")
    act_max_temp = table_tag.findAll(lambda tag: tag.name=='tr' and tag.find(lambda ttag: ttag.name=='td'))
    act_max_temp = act_max_temp[2].find(lambda tag: tag.string=='59')

    return [tag_1, prev_day, next_day, act_max_temp]

# Problem 5
def prob5(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    f = codecs.open(filename, 'r')
    soup = BeautifulSoup(f.read(), 'html.parser')
    date_links = []
    for elem in soup(text=re.compile(r'(.*)(((1[0-2]|0?[1-9])\/(3[01]|[12][0-9]|0?[1-9])\/(?:[0-9]{2})?[0-9]{2})|((Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\s+\d{1,2},\s+\d{4}))(.*)')):
        if elem.parent.has_attr('href'):
            date_links.append(elem.parent)  

    return date_links[1:]
    


# Problem 6
def prob6(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """
    f = codecs.open(filename, 'r')
    soup = BeautifulSoup(f.read(), 'html.parser')

    table = soup.find_all('table')
    print(len(table))
    df = pd.read_html(str(table[2]))[0]

    foreign_sort = df.sort_values(by=['Foreign Branches'], ascending=False)
    domestic_sort = df.sort_values(by=['Domestic Branches'], ascending=False)

    for_names = foreign_sort['Bank Name / Holding Co Name'][:7]
    dom_names = domestic_sort['Bank Name / Holding Co Name'][:7]

    for_vals = foreign_sort['Foreign Branches'][:7]
    dom_vals = domestic_sort['Domestic Branches'][:7]
    
    ax1 = foreign_sort.plot.barh(x = 'Foreign Branches', y = 'Bank Name / Holding Co Name')
    ax2 = domestic_sort.plot.barh(x = 'Domestic Branches', y = 'Bank Name / Holding Co Name')

