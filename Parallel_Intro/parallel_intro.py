# iPyParallel - Intro to Parallel Programming
from ipyparallel import Client
import numpy as np
import time
from matplotlib import pyplot as plt

# Problem 1
def initialize():
    """
    Write a function that initializes a Client object, creates a Direct
    View with all available engines, and imports scipy.sparse as spar on
    all engines. Return the DirectView.
    """
    client = Client()
    dview = client[:]
    dview.block = True
    dview.execute("import scipy.sparse as spar")
    return dview

# Problem 2
def variables(dx):
    """
    Write a function variables(dx) that accepts a dictionary of variables. Create
    a Client object and a DirectView and distribute the variables. Pull the variables back and
    make sure they haven't changed. Remember to include blocking.
    """
    client = Client()
    dview = client[:]
    dview.block = True
    dview.push(dx) 
    return dview


# Problem 3
def prob3(n=1000000):
    """
    Write a function that accepts an integer n.
    Instruct each engine to make n draws from the standard normal
    distribution, then hand back the mean, minimum, and maximum draws
    to the client. Return the results in three lists.
    
    Parameters:
        n (int): number of draws to make
        
    Returns:
        means (list of float): the mean draws of each engine
        mins (list of float): the minimum draws of each engine
        maxs (list of float): the maximum draws of each engine.
    """
    client = Client()
    dview = client[:]
    dview.block = True
    dview.execute('import numpy as np')
    dview.execute(f"draws = np.random.normal({n})")
    dview.execute('mins = np.min(draws)')
    dview.execute('maxes = np.max(draws)')
    dview.execute('means = np.mean(draws)')
    return dview['mins'], dview['maxes'], dview['means']


# Problem 4
def prob4():
    """
    Time the process from the previous problem in parallel and serially for
    n = 1000000, 5000000, 10000000, and 15000000. To time in parallel, use
    your function from problem 3 . To time the process serially, run the drawing
    function in a for loop N times, where N is the number of engines on your machine.
    Plot the execution times against n.
    """
    client = Client()
    n_list = [1000000, 5000000, 10000000, 15000000]
    prob_3_times = []
    serial_times = []
    for n in n_list:
        start = time.time()
        prob3(n)
        end = time.time()
        prob_3_times.append(end-start)
        #run serially
        start_2 = time.time()
        for _ in  range(len(client.ids)):
            draws = np.random.standard_normal(n)
            mins = np.min(draws)
            maxes = np.max(draws)
            means = np.mean(draws)
        end_2 = time.time()
        serial_times.append(end_2 - start_2)
    plt.plot(n_list, prob_3_times, label='Problem 3 Times')
    plt.plot(n_list, serial_times, label='Serial Times')

    print(serial_times, prob_3_times)

    plt.legend()
    plt.show()
    
# Problem 5
def parallel_trapezoidal_rule(f, a, b, n=200):
    """
    Write a function that accepts a function handle, f, bounds of integration,
    a and b, and a number of points to use, n. Split the interval of
    integration among all available processors and use the trapezoidal
    rule to numerically evaluate the integral over the interval [a,b].

    Parameters:
        f (function handle): the function to evaluate
        a (float): the lower bound of integration
        b (float): the upper bound of integration
        n (int): the number of points to use; defaults to 200
    Returns:
        value (float): the approximate integral calculated by the
            trapezoidal rule
    """
    domain = np.linspace(a, b, n)
    client = Client()
    dview = client[:]
    dview.block = True
    dview.execute('import numpy as np')
    solutions = dview.map(f, domain)

    return np.sum(solutions)

    
